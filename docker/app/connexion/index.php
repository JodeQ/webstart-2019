<?php

    //démarrage de la session
    session_start();

    // tableau des utiilsateurs
    $users = [
        ["id" => "john", "password" =>"john"],
        ["id" => "bruno", "password" =>"bruno59"],
        ["id" => "eloise", "password" =>"eloelo"]
    ];

    // Initialisation des variables
    $form_errors = [];

    // vérification des données du formulaire
    if ( isset($_POST['action']) && $_POST['action'] == 'authenticate' ) {
        // récupération de l'identifiant et du mot de passe saisi
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $password = isset($_POST['password']) ? $_POST['password'] : '';

        // Recherche dans le tableau des utilisateurs si l'identifiant et le mot de passe sont bons
        if (isset($id) && isset($password)) {
            // Si trouvé enregistrement de l'id dans la session
            foreach($users as $user) {
                if ($id == $user['id'] && $password == $user['password'])
                    $_SESSION['id'] = $id;
            }
            // sinon affichage d'une erreur : utilisateur non trouvé
            if ( !isset($_SESSION['id']) )
                $form_errors['id'] = 'Utilisateur non trouvé';
        }
    }

    if ( isset($_POST['action']) && $_POST['action'] == 'disconnect' ) {
        // Purge de la session
        $_SESSION = [];
    }

    $connected_user = isset($_SESSION['id']) ? $_SESSION['id'] : null;
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Formulaire de connexion</title>

    <style>
        .error {
            border: 1px solid red;
        }

        .error-message {
            font-size: smaller;
            color: red;
        }
    </style>
</head>

<body>
    <?php if (isset($connected_user)) : ?>
    <h1>Bienvenue <?php echo $connected_user; ?></h1>
    <form method="post">
        <input type="hidden" name="action" value="disconnect">
        <button type="submit">Déconnecter</button>
    </form>
    <?php else : ?>
    <form method="post" style="display: flex; align-items: center; flex-direction: column;">
        <input type="hidden" name="action" value="authenticate">
        <div>
            <label for="id">Identifiant : </label>
            <input type="text" name="id" id="id" <?php if (isset($form_errors["id"])) echo 'class="error"'; ?>>
            <?php if (isset($form_errors["id"])) echo '<span class="error-message">' . $form_errors["id"] . '</span>'; ?>
        </div>
        <div>
            <label for id="password">Mot de passe</label>
            <input type="password" name="password" id="password">
        </div>
        <button type="submit">Se connecter</button>
    </form>
    <?php endif; ?>
</body>
</html>