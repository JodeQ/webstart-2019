<?php

require_once('db.php');

//$insert_id = $db->lastInsertId(); // Retourne l'identifiant inséré par la requête (nécessite une clé primaire en AUTO_INCREMENT)

//var_dump($insert_id);

if (isset($_POST['form_action'])) {
    if ($_POST['form_action'] == 'add_user') {

        // Validation des données


        $query = $db->prepare('INSERT INTO utilisateur(nom, prenom, date_de_naissance, email, mot_de_passe) VALUES (:nom, :prenom, :date_de_naissance, :email, :mot_de_passe)' );
        $query->bindValue(':nom', $_POST['nom'], PDO::PARAM_STR);
        $query->bindValue(':prenom', $_POST['prenom'], PDO::PARAM_STR);
        $query->bindValue(':date_de_naissance', $_POST['date_de_naissance'], PDO::PARAM_STR);
        $query->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
        $query->bindValue(':mot_de_passe', $_POST['mot_de_passe'], PDO::PARAM_STR);
        
        $query->execute();
    }

    else if ($_POST['form_action'] == 'del_user') {
        $query = $db->prepare("DELETE FROM utilisateur WHERE id = :id");
        $query->bindValue(':id', $_POST['id'], PDO::PARAM_INT);
        $query->execute();
    }
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <form method="post"><!-- nom, prenom, date_de_naissance, email, mot_de_passe -->
            <div class="form-row">
                <input type="hidden" name="form_action" value="add_user">
                <div class="col">
                    <input type="text" name="nom" class="form-control" placeholder="Nom">
                </div>
                <div class="col">
                    <input type="text" name="prenom" class="form-control" placeholder="Prénom">
                </div>
                <div class="col">
                    <input type="date" name="date_de_naissance" class="form-control" placeholder="Date de naissance (YYYY-MM-DD)">
                </div>
                <div class="col">
                    <input type="email" name="email" class="form-control" placeholder="email">
                </div>
                <div class="col">
                    <input type="password" name="mot_de_passe" class="form-control">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <div class="container">
    <?php
    $query = $db->query("SELECT * FROM utilisateur");

    while($user = $query->fetch()) {
        //var_dump($user);
        echo '<div class="row">';
        echo '<div class="col"><form method="post"><input type="hidden" name="form_action" value="del_user"><input type="hidden" name="id" value="' . $user['id'] . '"><button class="btn" type="submit">x</button></form></div>';
        echo '<div class="col"><a class="btn btn-primary" href="update.php?id=' . $user['id'] . '">editer</a></div>';
        echo '<div class="col border border-primary">' . $user['id'] . '</div>';
        echo '<div class="col border border-primary">' . $user['nom'] . '</div>';
        echo '<div class="col border border-primary">' . $user['prenom'] . '</div>';
        echo '</div>';
    }
    ?>
    </div>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>

