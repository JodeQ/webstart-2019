
<?php

require_once('db.php');

// url : localhost:8080/boite/update.php?id=34232
$id = false;

if (isset($_GET['id'])) {
    // Fixer la valeur de $id
    $id = $_GET['id'];

    if (isset($_POST['form_action'])) {
        if ($_POST['form_action'] == 'update_user') {
            // Validations des champs

            $query = $db-> prepare('UPDATE utilisateur 
             SET nom = :nom,
             prenom = :prenom,
             date_de_naissance = :date_de_naissance,
             email= :email,
             mot_de_passe = :mot_de_passe
             WHERE id = :id');
            $query->bindValue(':nom', $_POST['nom'], PDO::PARAM_STR);
            $query->bindValue(':prenom', $_POST['prenom'], PDO::PARAM_STR);
            $query->bindValue(':date_de_naissance', $_POST['date_de_naissance'], PDO::PARAM_STR);
            $query->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
            $query->bindValue(':mot_de_passe', $_POST['mot_de_passe'], PDO::PARAM_STR);
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            
            if ($query->execute()) {
                // Rediriger vers la page d'index
                // Attention cette instruction doit se faire avant d'écrire quoi que ce soit, pas de echo ni de var_dump avant cette ligne
                header('Location: index.php');
                exit();
            }
        }
    }

    $query = $db->prepare('select * from utilisateur where id = :id');
    $query->bindValue(':id', $id);
    $query->execute();

    // Affectation de $user avec le résultat de $query->fetch
    // Si après l'affectation $user = false alors on rentre dans le if et $id est remis à false
    if (!$user = $query->fetch())
        $id = false;

}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>
    <?php if (!$id) : ?>
        <h2>Utilisateur non reconnu</h2>
    <?php else : ?>

    <div class="container">
        <form method="post"><!-- nom, prenom, date_de_naissance, email, mot_de_passe -->
            <div class="form-row">
                <input type="hidden" name="form_action" value="update_user">
                <div class="col">
                    <input type="text" name="nom" class="form-control" placeholder="Nom" value="<?php echo $user['nom']; ?>">
                </div>
                <div class="col">
                    <input type="text" name="prenom" class="form-control" placeholder="Prénom" value="<?php echo $user['prenom']; ?>">
                </div>
                <div class="col">
                    <input type="date" name="date_de_naissance" class="form-control" placeholder="Date de naissance (YYYY-MM-DD)" value="<?php echo $user['date_de_naissance']; ?>">
                </div>
                <div class="col">
                    <input type="email" name="email" class="form-control" placeholder="email" value="<?php echo $user['email']; ?>">
                </div>
                <div class="col">
                    <input type="password" name="mot_de_passe" class="form-control" value="<?php echo $user['mot_de_passe']; ?>">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <?php endif; ?>

</body>
</html>