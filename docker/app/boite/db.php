<?php

define('HOST', 'localhost'); // Domaine ou IP du serveur ou est située la base de données
define('USER', 'admin'); // Nom d'utilisateur autorisé à se connecter à la base
define('PASS', 'l5aHEvUvouWM'); // Mot de passe de connexion à la base
define('DB', 'boite'); // Base de données sur laquelle on va faire les requêtes

// Tableau d'options supplémentaires pour la connexion
$db_options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8", // On force l'encodage en utf8
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, // On récupère tous les résultats en tableau associatif
    PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING // On affiche des warnings pour les erreurs, à commenter en prod (valeur par défaut PDO::ERRMODE_SILENT)
);

// On crée la connexion à la base de données
try {
    $db = new PDO('mysql:host='.HOST.';dbname='.DB, USER, PASS, $db_options);
} catch (Exception $e) {
  error_log('['.$e->getCode().'] '.$e->getMessage(), 3, 'logs/mysql-errors.log');
}


