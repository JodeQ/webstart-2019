<?php

echo "Exercices de PHP<br>\n";

// Ceci est un commentaire

// Déclaration de la variable a
$hello = 'Hello World !';

echo $hello;

$saut = "<br>\n";
echo $saut;

echo "test de concaténation" . $saut;

$x = 4;
$double = $x + $x;
echo "le double de $x est : " . $double . $saut;

$carre = $x * $x;
echo 'le "carré" de '. $x . ' est : ' .$carre . $saut;
echo "le \"carré\" de $x est : " .$carre . $saut;



$x = rand(-10, 10);

// le if (si)
if ($x > 0) {
    echo "$x est supérieur à 0" . $saut;
} else {
    echo "$x est inférieur ou égal à 0" . $saut;
}


$hello = "Bonjour ";
$hello .= " le monde !";   // $hello = $hello . " le monde !";
$hello .= $saut;
echo $hello;


// La boucle while
$x = rand(-10, 10);
while($x >= 0) {
    echo "$x est positif" . $saut;
    $x = rand(-10, 10); // on n'oublie pas d'actualiser $x
}
echo "$x est négatif" . saut;

// les tableaux
$fruits = array('Pomme', 'Banane', 'Orange');
$enfants = ['Tom', 'Léa', 'Louis'];

echo $enfants[2] . "mange une " . $fruits[1] . $saut;

echo $saut . "<h3>Exercice de tableau : </h3>" . $saut;
foreach($enfants as $enfant) {
    $phrase = $enfant . " mange une ";
    foreach ($fruits as $fruit) {
        echo $phrase . $fruit . $saut;
    }
}


?>