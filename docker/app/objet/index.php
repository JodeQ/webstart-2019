<?php

/*
 * PHP est un language qui permet d'érire du code Orienté Objet
 * ==> POO (Programmation Orientée Objet)
 */

// Dossier des classes : classes 
require_once "classes/utilisateur.class.php";
require_once "classes/admin.class.php";

/**
 * "new" permet d'instancier une classe dans un objet
 */
$bruno = new Admin('Bruno', '1143332', 'Sud');
$mathilde = new Utilisateur('Mathilde', '1234', 'Nord');
$florian = new Utilisateur('Flo', 'flotri', 'Est');

$bruno->setPrixAbo();
$mathilde->setPrixAbo();
$florian->setPrixAbo();

echo $bruno->getNom2() . '<br>';
echo $bruno->getNom() . '<br>';
echo $mathilde->getNom() . '<br>';

$bruno->setBan('Paul');
$bruno->setBan('Pierre');

$bruno->getBan();

echo 'Prix de l\'abonnement pour : <br><ul>';
echo '<li>' . $bruno->getNom2() . ' : ' . $bruno->getPrixAbo() . '</li>'; 
echo '<li>' . $mathilde->getNom() . ' : ' . $mathilde->getPrixAbo() . '</li>';
echo '<li>' . $florian->getNom() . ' : ' . $florian->getPrixAbo() . '</li></ul>';


$u = 'Utilisateur';
echo "Valeur de l'abonnement dans Utilisateur : " . $u::ABONNEMENT . '<br>';
echo "Valeur de l'abonnement dans Admin : " . Admin::ABONNEMENT . '<br>';


/*$bruno->setName('Bruno');
$bruno->setPass('12233434');

$mathilde->setName('Mathilde');
$mathilde->setPass('56563');
*/
var_dump($bruno);

?>