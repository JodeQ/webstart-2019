<?php
/**
 * Classe des utilisateurs
 */
class Utilisateur {

    /**
     * Constante de classe
     */
    public const ABONNEMENT = 15;

    /**
     * Propriétés de classe (variable)
     */
    protected $user_name;
    protected $user_pass;
    protected $prix_abo;
    protected $user_region;

    /**
     * Méthodes de classe (fonction)
     */

    /**
     * Constructeur
     */
    public function __construct($user_name, $user_pass, $user_region) {
        $this->user_name = $user_name;
        $this->user_pass = $user_pass;
        $this->user_region = $user_region;
    }

    /**
     * Destructeur
     */
    public function __destruct() {
        null;
    }

    /**
     * Getters
     */
    public function getNom() {
        return $this->user_name;
    }

    public function getPrixAbo(){
        return $this->prix_abo;
    }

    /**
     * Setters
     */
    public function setName($new_user_name) {
        $this->user_name = $new_user_name;
    }
    public function setPass($new_user_pass) {
        $this->user_pass = $new_user_pass;
    }
    
    public function setPrixAbo() {
        // on calcule le prix d'un abonnement selon le profil de l'utilisateur
        if ($this->user_region === 'Sud') {
            $this->prix_abo = self::ABONNEMENT / 2;
        } else {
            $this->prix_abo = self::ABONNEMENT;
        }
    }
}