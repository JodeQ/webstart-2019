<?php

require_once "utilisateur.class.php";

class Admin extends Utilisateur {

    public const ABONNEMENT = 5;

    protected $ban;

    public function setBan($ban) {
        $this->ban[] .= $ban;
    }

    public function getBan(){
        echo "Utilisateurs bannis par " . $this->user_name . ' : ';
        foreach($this->ban as $ban) {
            echo $ban . ', ';
        }
    }

    public function getNom2() {
        return $this->user_name;
    }

    /**
     * Surcharge d'une fonction (redéfinition)
     */
    public function getNom() {
        //  :: est l'opérateur de résolution de portée
        $name = parent::getNom();
        return '<h1>' . $name . '</h1>';
    }

    public function setPrixAbo() {
        if ($this->user_region === 'Sud') {
            $this->prix_abo = self::ABONNEMENT;
        } else {
            $this->prix_abo = parent::ABONNEMENT / 2;
        }
    }
}