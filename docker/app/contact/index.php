<?php
    // Ajout du fichier des fonctions
    require_once('./functions.php');

    // Démarrage de la session
    session_start();

    // test action
    if (postValue('action') == 'reset') {
        // Vider le tableau de session
        $_SESSION = [];
    }

    // S'il n'y a pas d'id_user
    if (!isset($_SESSION['id_user'])) {
        $_SESSION['id_user'] = '1234567';
        echo "id utilisateur ajouté";
    }

    // Condition ? alors : sinon
    !isset($_SESSION['nb_visite']) ? $_SESSION['nb_visite'] = 1 : $_SESSION['nb_visite'] += 1;

    var_dump($_SESSION);
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Formulaire de contact</title>

    <style>
        .error {
            border: 1px solid red;
        }

        .error-message {
            font-size: smaller;
            color: red;
        }
    </style>
</head>

<body>
    <?php
    var_dump($_POST);

    // tableau des erreurs du formulaire
    $form_errors = [];

    function validate()
    {
        global $form_errors;

        // tester nos entrées
        // Le nom ne doit pas être vide
        if (postValue("nom") == "")
            $form_errors["nom"] = "Le nom doit être renseigné";
        
        // Le prénom ne doit pas être vide

        // L'email doit être un email valide
        if (postValue('email') == '')
            $form_errors['email'] = "l'email doit être renseigné";
        elseif (!filter_var(postValue('email'), FILTER_VALIDATE_EMAIL))
            $form_errors['email'] = "l'email n'est pas valide";

        // La description doit faire au moins 20 caractères de long

        // La profession doit être une valeur valide (étudiant, salarié)

    }

    if (postValue('action') == 'validate')
        validate();
    
    var_dump($form_errors);

    ?>

    <form method="post">
        <div>
            <label for="id_nom">Nom : </label>
            <input id="id_nom" name="nom" type="text" value="<?php echo postValue('nom') ?>" <?php if (isset($form_errors["nom"])) echo 'class="error"'; ?>>
            <?php if (isset($form_errors["nom"])) echo '<span class="error-message">' . $form_errors["nom"] . '</span>'; ?>
        </div>
        <div>
            <label for="id_prenom">Prénom : </label>
            <input id="id_prenom" name="prenom" type="text" value="<?php if (isset($_POST['prenom'])) echo $_POST['prenom']; ?>">
        </div>
        <div>
            <label for="id_email">Email : </label>
            <input id="id_email" name="email" type="email" value="<?php if (isset($_POST['email'])) echo $_POST['email']; ?>">
        </div>
        <div>
            <label for="id_description">Description : </label>
            <textarea id="id_description" name="description" rows="5" cols="80" style="vertical-align: top;" placeholder="Description de base"><?php if (isset($_POST['description'])) echo $_POST['description']; ?></textarea>
        </div>
        <div>
            <label for=id_profession">Profession : </label>
            <select id="id_profession" name="profession">
                <option value="" <?php if (isset($_POST['profession']) && $_POST['profession'] == '') echo "selected"; ?>></option>
                <option value="étudiant" <?php if (isset($_POST['profession']) && $_POST['profession'] == 'étudiant') echo "selected"; ?>>étudiant</option>
                <option value="salarié" <?php if (isset($_POST['profession']) && $_POST['profession'] == 'salarié') echo "selected"; ?>>salarié</option>
            </select>
        </div>
        <div>
            <input id="id_subscribe" name="subscribe" type="checkbox" <?php if (isset($_POST['subscribe']) && $_POST['subscribe'] == 'on') echo "checked"; ?>>
            <label for="id_subscribe">Recevoir newsletter</label>
        </div>
        <div>
            <input type="submit">
        </div>
        <input type="hidden" name="action" value="validate">
    </form>

    <form method="post">
        <input type="hidden" name="action" value="reset">
        <button type="submit">Reset</button>
    </form>

</body>

</html>