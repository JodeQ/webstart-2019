<?php

/**
 * Récupère la valeur dans $_POST
 * @param string $value La clé dans le tableau $_POST
 * @param string $default La valeur par défaut si $_POST['$value'] est vide
 * @return string La valeur du champ $_POST ou la valeur par défaut 
*/
function postValue($value, $default = '')
{
    return !empty($_POST[$value]) ? $_POST[$value] : $default;

    /* équivalent à :
        if (!empty($_POST[$value]))
            return $_POST[$value];
        else
            return $default;
        */
}