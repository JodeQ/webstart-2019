<?php

// morpion
$carte = array();

//                col1, col2, col3
$carte[0] = array('X',  'O',  'X');  // ligne 1
$carte[1] = array('X',  'O',  'O');  // ligne 2
$carte[2] = array('O',  'O',  'X');  // ligne 3

echo "En position ligne 2, colonne 3 : " . 
     $carte[1][2] . "<br>\n";

echo "En position ligne 3, colonne 3 : " . 
     $carte[2][2] . "<br>\n";
    

$personnes = array(
    '0' => array(
        'nom' => 'Dupont',
        'prenom' => 'Pierre',
        'email' => 'pierre.d@gmail.com',
        'telephones' => array(
            'fixe' => '03 00 00 00 00',
            'portable' => '06 00 00 00 00'
        )
    ),
    '1' => array(
        'nom' => 'Dupont',
        'prenom' => 'Jean',
        'email' => 'jean.d@gmail.com',
        'telephones' => array(
            'fixe' => '03 00 00 00 00',
            'portable' => '06 00 00 00 00'
        )
    ),
    '2' => array(
        'nom' => 'Dupont',
        'prenom' => 'Marie',
        'email' => 'marie.d@gmail.com',
    )
);

var_dump($personnes);

foreach($personnes as $personne) {
    echo 'Nom : '.$personne['nom'].'<br/>';
    echo 'Prénom : '.$personne['prenom'].'<br/>';
    echo 'Email : '.$personne['email'].'<br/>';

    // On vérifie l'existence de la cellule des téléphones
    if (isset($personne['telephones'])) {
        foreach($personne['telephones'] as $telephone) {
            echo 'Téléphone : '.$telephone.'<br/>';
        }
    }
}


echo "<br>\nFonctions sur les chaînes<br>\n";

// fonctions sur les chaines
$message  = 'email@domain.com';
echo strpos($message, '@') ."<br>\n"; // Affiche 5
if (!strpos($message, '€'))
    echo "€ non trouvé" ."<br>\n"; 

$message = '<h1>Hellow World, </h1><p>How are you ?</p>';
echo strip_tags($message); //Affiche 'Hellow World, How are you ?'
echo strip_tags($message, '<h1>');

$str = "O'Reilly?";
eval("echo '" . addslashes($str) . "';");
    
?>