<?php
    require_once 'classes/pokemon.class.php';

    $pikachu = new Pokemon(1, "Pikachu", "pikapika", 132, 10, 10, 50);

    $bulbizarre = new Pokemon(2, "Bulbizarre", "bulbi", 154, 15, 10, 40);

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pokedex</title>
</head>
<body>
    <?php
        echo "Début du combat<br>";
        while ($pikachu->getPv() > 0 && $bulbizarre->getPv() > 0) {
            echo "Pikachu attaque !<br>";
            $bulbizarre->defense($pikachu->attack());
            echo "Il reste " . $bulbizarre->getPv() . " points de vie à Bulbizarre<br>";
            if ($bulbizarre->getPv() > 0) {
                echo "Bulbizarre attaque !<br>";
                $pikachu->defense($bulbizarre->attack());
                echo "Il reste " . $pikachu->getPv() . " points de vie à Pikachu<br>";
            }
        }
        echo "Fin du combat : <br>";
        echo "Pikachu : " . $pikachu->getPv() . "<br>";
        echo "Bulbizarre : " . $bulbizarre->getPv() . "<br>";
    ?>
</body>
</html>