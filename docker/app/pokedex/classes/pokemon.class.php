<?php

class pokemon {

    /**
     * Propriétés
     */
    protected $id;
    protected $type;
    protected $name;
    protected $base_experience;
    protected $height;
    protected $weight;
    protected $attack;
    protected $defense;
    protected $pv;

    /**
     * Espèces
     */
    protected $species;

    public function __construct($id, $type, $name, $base_experience, $attack, $defense, $pv) {
        $this->id = $id;
        $this->type = $type;
        $this->name = $name;
        $this->base_experience = $base_experience;
        $this->attack = $attack;
        $this->defense = $defense;
        $this->pv = $pv;
    }

    public function attack() {
        return $this->attack;
    }

    public function defense($attack) {
        $this->pv = $this->pv - $attack;
    }

    public function getPv() {
        return $this->pv;
    }

}