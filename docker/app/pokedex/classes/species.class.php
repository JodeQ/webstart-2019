<?php

class PokemonSpecies {

    // Bulbizare
    public const BULBASAUR = 1;
    // Herbizarre
    public const IVYSAUR = 2;
    // Florizarre
    public const VENUSAUR = 3;
    // Salamèche
    public const CHARMANDER = 4;
    // Reptincel
    public const CHARMELEON = 5;
    // Dracaufeu
    public const CHARIZARD = 6;
    
}