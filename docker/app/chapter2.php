<?php

$array = array('ma cle' => 'ma valeur');

var_dump($array);

// Génère une erreur 'undefined offset'
// car l'indice (offset) 5 n'est pas déclaré
echo $array[5];
$array[5] = "mon autre valeur";
echo $array[5];

var_dump($array);

/* 
 * Sans clé, le tableau prend la valeur 
 * entière la plus grande et ajoute 1
 */
$array[] = 'une troisième valeur';
var_dump($array);

echo time() . "<br>\n";
echo date('d/m/Y') . "<br>\n";


/* isset() */
if (!isset($a)) {
    $a = 42; // Si la variable $a n'existe pas déjà, on la défini avec comme valeur 42.
}

/* empty() */
if (!empty($a)) {
    echo '$a =' . $a . "<br>\n"; // Si la variable $a n'est pas vide, on affiche sa valeur.
}

/* unset() */
unset($a); // Maintenant qu'on a défini puis utilisé la variable $a, on la détruit.
echo 'Après "unset($a)"' . "<br>\n";
var_dump($a);

/* time(); */
$t = time(); // affecte à $t le nombre de secondes écoulées depuis le 01/01/1970 à l'instant ou l'instruction est exécutée.

/* date() */
echo date('d / m / Y', $t) . "<br>\n"; // Affichera la date au format JJ / MM / AAAA, par exemple 21 / 10 / 2015.

/* count() */
$b = array('pomme', 'poire', 'abricot');
echo count($b) . "<br>\n"; // Affichera 3, car $b contient 3 valeurs.

/* strlen() */
$c = 'Hello World !';
echo strlen($c) . "<br>\n"; // Affichera 13, car $c contient 13 caractères (cela inclut les espaces)

echo date('H\h i\m s\s') . "<br>\n";

$the_date = strtotime('9 january 2007');
echo date('d/m/Y', $the_date) . "<br>\n";
?>